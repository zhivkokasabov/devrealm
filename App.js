import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';

import Input from './Components/Input'
import Modal from './Components/Modal'

const STORAGE_KEY = 'pin'

export default class App extends React.Component {
  constructor(props) {
    super(props);

    // this.resetPin()

    this.state = {
      placeholder: 'This is your first log in, type in your pin!',
      pin: null,
      login: false,
      showModal: false
    };
  }

  resetPin = async () => {
    await AsyncStorage.setItem(STORAGE_KEY, '');
  }

  componentWillMount = async () => {
    try {
      const pin = await AsyncStorage.getItem(STORAGE_KEY);

      if (pin) {
        this.setState({
            pin,
            placeholder: 'Enter your pin to log in!',
            login: true
        })
      }
    } catch(e) {
      console.error(e);
    }
  }

  save = async (pin) => {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, pin)

      this.setState({
        pin,
        placeholder: 'Enter your pin to log in!',
        message: 'Your pin has been saved!',
        showModal: true,
        login: true
      })
    } catch (e) {
      console.error(e);
    }
  }

  closeModal = () => {
    this.setState({
      showModal: false
    })
  }

  modal = () => {
    if (this.state.showModal) {
      return (
        <Modal
          closeModal={this.closeModal}
          message={this.state.message}
        />
      );
    }
  }

  login = async (input) => {
    const pin = await AsyncStorage.getItem(STORAGE_KEY);

    if (input === pin) {
      this.setState({
        message: 'An albanian hacker cracked your password, you should definetly change it',
        placeholder: 'Hurry up before he finds the hidden folders',
        login: false
      })
    } else {
      this.setState({
        message: 'Wrong pin, try again!'
      })
    }

    this.setState({
      showModal: true
    })
  }

  render() {
    const fn = (this.state.login) ? this.login : this.save
    const modalView = this.modal()

    return (
      <View style={styles.container}>
        <Input
          placeholder={this.state.placeholder}
          onSubmitEditing={fn}
          pin={this.state.pin}
        />
        {modalView}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
