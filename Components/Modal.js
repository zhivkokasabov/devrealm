import React, { Component } from 'react'
import { TextInput, Text, Button, StyleSheet, View } from 'react-native'

export default class Modal extends Component {

  constructor(props){
    super(props);
    this.state = {
      message: ''
    };
  }

  render() {
    const pin= this.state.pin

    return (
      <View
        style={styles.content}
      >
        <View style={styles.modal}>
          <View style={styles.message}>
            <Text style={styles.text}>
              {this.props.message}
            </Text>
          </View>
          <Button
          style={styles.button}
            onPress={this.props.closeModal}
            title="LOGIN"
            color="#841584"
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',

    backgroundColor: 'rgba(22, 22, 22, 0.9)',
  },
  modal: {
    width: 200,
    backgroundColor: '#fafafa',
    borderWidth: 1,
    borderColor: '#d6d7da',
    alignSelf: 'center'
  },
  button: {
    height: 40
  },
  message: {
    height: 60,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  text: {
    alignSelf: 'center',
    textAlign: 'center'
  }
});
