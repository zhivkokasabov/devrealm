import React, { Component } from 'react'
import { TextInput, StyleSheet } from 'react-native'

export default class Input extends Component {

  constructor(props){
    super(props);
    this.state = {
      pin:''
    };
  }

  onChangeText = (pin) => this.setState({pin})

  onSubmitEditing = () => {
    const { onSubmitEditing } = this.props
    const { pin } = this.state

    onSubmitEditing(pin);

    this.setState({
      pin: ''
    })
  }

  render() {
    const pin= this.state.pin

    return (
      <TextInput
        style={styles.input}
        value={ pin }
        placeholder={this.props.placeholder}
        onChangeText={this.onChangeText}
        onSubmitEditing={this.onSubmitEditing}
      />
    )
  }
}

const styles = StyleSheet.create({
  input: {
    padding: 15,
    height: 50,
    width: '100%'
  }
});
